/**
 * Game module containing all game methods & logic
 * @alias gameCoreModule
 * @returns {Object} - All functions available outside
 */
var gameCoreModule = (function () {
  // Game actions
  /**
   * Create & render a new game, deleting existing one
   * @alias createNewGame
   */
  function createNewGame() {
    clearIntervalTimeCounter();
    game = wipeCurrentGame();

    fillPlayingBoard();
  }

  /**
   * Start current game
   * Start the game, start counter & config mines
   * @alias startGame
   */
  function startGame() {
    game.isPlaying = true;
    game.timer.interval = setInterval(startTimeCounter, 1000);

    // Create game state
    var indices = []
    for (var i = 0; i < game.playingBoard.rows; ++i) {
      for (var j = 0; j < game.playingBoard.columns; ++j) {
        var tIndex = i * game.playingBoard.columns + j;
        indices.push(tIndex);

        game.playingBoard.squares[tIndex] = {
          isHidden: true,
          isMine: false,
          isMarked: false,
          number: 0
        }
      }
    }

    addMines(indices);
  }

  /**
   * Restart current game, deleting timer and creating a new game
   * @alias restartGame
   */
  function restartGame() {
    clearIntervalTimeCounter();
    resetTimeCounter();

    createNewGame();
  }

  /**
   * Wipe the entire game, resetting every config to normal
   * @alias wipeCurrentGame
   * @returns {Object} - new game config
   */
  function wipeCurrentGame() {
    return {
      isPlaying: false,
      isNormalClickMarker: false,
      playingBoard: {
        rows: gameSettings.rows,
        columns: gameSettings.columns,
        mines: [],
        squares: []
      },
      timer: {
        value: 0,
        interval: null
      }
    }
  }

  /**
   * Dynamicaly generate a new square, based on device screen size
   * Add style for square
   * Add events for clicking on square
   * @alias createNewSquare
   * @param {Object} squareVisualConfig - Visual configuration for square, based on device screen size
   * @param {number} id - ID for square on the playing board, it will be part of it's HTML tag id (EXP: id = "square_1")
   * @returns {HTMLElement} square - The new created sqaure
   */
  function createNewSquare(squareVisualConfig, id) {
    var square = document.createElement("div");
    square.id = "square_" + (id);
    square.className = "square";
    square.style.width = squareVisualConfig.width + "px";
    square.style.height = squareVisualConfig.height + "px";
    square.addEventListener("mousedown", registerSqaureClick);
    // Remove default behavior for right click on squares
    square.addEventListener('contextmenu', function (event) {
      event.preventDefault()
    });
    return square;
  }
  /**
   * Generate new Square Visual, based on screen size
   * @alias getNewSquareVisual
   * @param {HTMLElement} parent - Main element that contains the game for getting the screen size where square should be rendered
   * @returns {Object} config - The congifuration that was created
   */
  function getNewSquareVisual(parent) {
    var config = {
      width: "40px",
      height: "40px"
    }
    var bounds = parent.getBoundingClientRect();
    if (bounds) {
      // -2.5 px for margin on all faces
      config.width = bounds.width / game.playingBoard.columns - 10;
      config.height = bounds.height / game.playingBoard.rows - 10;

      // Remake squareVisual a sqaure
      if (config.width < config.height) {
        config.height = config.width;
      } else {
        config.width = config.height;
      }
    }

    return config;
  }

  /**
   * Rerender the playboard from scratch based on new confgis & new screen size
   * @alias fillPlayingBoard
   */
  function fillPlayingBoard() {
    var gameMain = document.getElementById("game_main");
    utilsModule.emptyElement(gameMain);

    // Create new container
    var container = document.createElement("div");
    container.className = "square_array";
    // remove context menu on row
    container.addEventListener('contextmenu', function (event) {
      event.preventDefault()
    });
    // Adjust square to game settings
    var squareVisualConfig = getNewSquareVisual(gameMain);

    for (var i = 0; i < game.playingBoard.rows; ++i) {
      var row = document.createElement("div");
      row.className = "square_row";

      for (var j = 0; j < game.playingBoard.columns; ++j) {
        var square = createNewSquare(squareVisualConfig, i * game.playingBoard.columns + j);
        row.appendChild(square);
      }
      container.appendChild(row);
    }

    gameMain.appendChild(container);
  }

  /**
   * Add randomly placed mines to playboard
   * Number of mines is based on the difficulty cohsen in game settings
   * Formula: abs((difficulty * 2 + 10) / totalNumberOfSquares / 100)
   * @alias addMines
   * @param {Array} allSquaresIds - allowed squares id for placeing mines 
   */
  function addMines(allSquaresIds) {
    var numberOfMines = Math.floor((gameSettings.difficulty * 2) * game.playingBoard.rows * game.playingBoard.columns / 100);

    // At least one mine
    if (numberOfMines < 1) {
      numberOfMines = 1
    }

    game.playingBoard.mines = utilsModule.shuffle(allSquaresIds).slice(0, numberOfMines);
    game.playingBoard.mines.forEach(function (id) {
      game.playingBoard.squares[id].isMine = true;

      // Fill neighbours with numbers for coresponding mine
      getAllNeighbours(id).forEach(function (sid) {
        game.playingBoard.squares[sid].number++;
      })
    });
  }

  /**
   * Register click event on mouse press and try to interact with playboard squares
   * Everytime when a player clicks on a square try to check if game is completed
   * @alias registerSqaureClick
   * @param {Event} event - Click event
   */
  function registerSqaureClick(event) {
    var tar = event.target;
    var sid = tar.id.split("_")[1];

    // In case of clicking inside div
    if (!sid) {
      tar = tar.parentElement;
      sid = tar.id.split("_")[1];
    }

    if (!game.isPlaying) {
      createNewGame();
      startGame();
    }

    // Mark the square otherwise reaveal it
    if (event.which == 3 || game.isNormalClickMarker) {
      tryMarkSquare(sid, document.getElementById("square_" + sid));
    } else {
      tryRevealSquare(sid, document.getElementById("square_" + sid));
    }
    game.isNormalClickMarker = false;
    
    checkWinGame();
  }

  /**
   * Run CSS Animation on square flip
   * @alias squareRunAnimation
   * @param {HTMLElement} square - Square's HTML Element
   * @param {Function} square - Callback invoked after animation ends
  */
  function squareRunAnimation(square, cb, newClassName) {
    // Remove all square anims
    square.className = square.className.replace(/ *\b\S*?square_anim\S*\b/g, '')
    square.className = square.className + " square_anim";
    square.addEventListener("animationend", function (event) {
      if (event.animationName == "square_flip") {
        cb(square, newClassName);
        // Remove all square anims
        square.className = square.className.replace(/ *\b\S*?square_anim\S*\b/g, '')
        square.className = square.className + " square_anim_reverse";
      }
    }, false);
  }

  /**
   * Try to reveal unrevealed squares
   * If a player clicks on a mine, dispach a lose event
   * If a player clicks on an empty square, reveal everything that's nearby and also empty or square with number, recursivly
   * If a player clicks on a square with number, reveal it
   * @alias tryRevealSquare
   * @param {number} id - Square ID that we want to try flipping 
   * @param {HTMLElement} square - Square Element that was originally clicked
   */
  function tryRevealSquare(id, square) {
    var currentSquareState = game.playingBoard.squares[id];

    if (currentSquareState.isHidden && !currentSquareState.isMarked) {
      if (currentSquareState.isMine) {
        loseGame();
        squareRunAnimation(square, function (sq) {
          sq.className = "square mine";
        });
      } else {
        if (currentSquareState.number == 0) {
          reavealSquaresNearby(id, true);
          squareRunAnimation(square, function (sq) {
            sq.className = "square revealed";
          });
        }
        else {
          revealSquareWithNumber(square, currentSquareState);
          // reavealSquaresNearby(id, true);
        }
      }

      currentSquareState.isHidden = false;
    }
  }

  /**
   * Try to mark/unmark an unrevealed square
   * @alias tryMarkSquare
   * @param {number} id - Square ID that we want to try flipping 
   * @param {HTMLElement} square - Square Element that was originally clicked
   */
  function tryMarkSquare(id, square) {
    var currentSquareState = game.playingBoard.squares[id];

    // If the square is hidden, flip isMarked
    if (currentSquareState.isHidden) {
      currentSquareState.isMarked = !currentSquareState.isMarked;
      if (currentSquareState.isMarked) {
        squareRunAnimation(square, function (sq) {
          sq.className = "square flagged";
        });
      } else {
        squareRunAnimation(square, function (sq) {
          sq.className = "square";
        });
      }
    }
  }

  /**
   * Lose event
   * Stop timers, add lose dialog and reveal all mines in game
   * @alias loseGame
   */
  function loseGame() {
    game.isPlaying = false;
    clearIntervalTimeCounter();
    createLoseDialog();

    game.playingBoard.mines.forEach(function (mineId) {
      squareRunAnimation(document.getElementById("square_" + mineId), function (sq) {
        sq.className = "square mine mine_autorevealed";
      });
    });
  }

  /**
   * Create lose dialog
   * @alias createLoseDialog
   */
  function createLoseDialog() {
    var contentDialog = document.createElement("div");
    var score = document.getElementById("game_counter").innerText;
    contentDialog.appendChild(document.createTextNode("Game Over. Better luck next time! Your score: " + score));
    var cancelButtonElem = document.createElement("button");
    cancelButtonElem.className = "button_cancel";
    cancelButtonElem.onclick = dialogModule.removeDialog;
    cancelButtonElem.appendChild(document.createTextNode("Cancel"));
    dialogModule.enableDialog("You Lost", [contentDialog], [cancelButtonElem])
  }

  /**
   * Get All Neighbours IDs of a square
   * @alias getAllNeighbours
   * @param {number} id - Square id for who to check for
   * @returns {Array} res - An array of numbers, containing all neighours IDs
   */
  function getAllNeighbours(id) {
    var res = [];

    var currentRow = Math.floor(id / game.playingBoard.columns);
    var currentColumn = id % game.playingBoard.columns;

    for (var k = -1; k <= 1; k++) {
      if ((currentRow + k) >= 0 && (currentRow + k) < game.playingBoard.rows) {
        for (var l = -1; l <= 1; l++) {
          if ((currentColumn + l) >= 0 && (currentColumn + l) < game.playingBoard.columns) {
            var temp = (currentRow + k) * game.playingBoard.columns + currentColumn + l;
            if (temp != id)
              res.push(temp);
          }
        }
      }
    }

    return res;
  }

  /**
   * Recursive function for revealing all nearby squares
   * @alias reavealSquaresNearby
   * @param {number} id - Square id
   * @param {boolean} recursively - Check recurssivly or not 
   */
  function reavealSquaresNearby(id, recursively) {
    var neigh = getAllNeighbours(id);

    // Reveal only if it's not hidden
    if (game.playingBoard.squares[id].isHidden) {
      game.playingBoard.squares[id].isHidden = false;

      // For every neighbour try to reveal
      neigh.forEach(function (nid) {
        var currentSquareState = game.playingBoard.squares[nid];
        var square = document.getElementById("square_" + nid);

        if (square) {
          // Ignore mines
          if (!currentSquareState.isMine) {
            // If it' an empty square, reveal and click on all neighbours
            if (currentSquareState.number == 0) {
              squareRunAnimation(square, (function (sq) {
                sq.className = "square revealed";
              }));
              if (recursively)
                reavealSquaresNearby(nid, recursively);
            }
            // If it's not empty, reveal only the number in the box
            else {
              revealSquareWithNumber(square, currentSquareState);
            }
            currentSquareState.isHidden = false;
          }
        }
      });
    }
  }

  /**
   * Reveal square that is a number
   * @alias revealSquareWithNumber
   * @param {HTMLElement} squareElem - Square element
   * @param {Object} currentState - current state of square
   */
  function revealSquareWithNumber(squareElem, currentState) {
    squareRunAnimation(squareElem, (function (sq, newClassName) {
      sq.className = newClassName;
      var span = document.createElement("span");
      span.appendChild(document.createTextNode(currentState.number));
      span.className = "square_content";
      while (sq.firstChild) {
        sq.removeChild(sq.firstChild);
      }
      sq.appendChild(span);
    }), "square revealed number_" + currentState.number);

    currentState.isHidden = false;
  }

  /**
   * Increment time counter
   * @alias startTimeCounter
   */
  function startTimeCounter() {
    document.getElementById("game_counter").innerText = ++game.timer.value;
  }

  /**
   * Reset time counter
   * @alias resetTimeCounter
   */
  function resetTimeCounter() {
    game.timer.value = 0;
    document.getElementById("game_counter").innerText = game.timer.value;
  }

  /**
   * Remove interval for time counter
   * @alias clearIntervalTimeCounter
   */
  function clearIntervalTimeCounter() {
    clearInterval(game.timer.interval);
    game.timer.interval = null;
  }

  /**
   * Win State
   * @alias checkWinGame
   */
  function checkWinGame() {
    var notHiddenSquares = 0;
    var markedSquares = 0;

    // Count
    game.playingBoard.squares.forEach(function (sq) {
      if (!sq.isHidden) {
        notHiddenSquares++;
      }
      if (sq.isMarked) {
        markedSquares++;
      }
    })

    // Check for win
    if (markedSquares == game.playingBoard.mines.length &&
      notHiddenSquares + markedSquares == game.playingBoard.rows * game.playingBoard.columns) {
      winGame();
    }
  }

  /**
   * Create win dialog
   * @alias createWinDialog
   */
  function createWinDialog() {
    var contentDialog = document.createElement("div");
    var score = document.getElementById("game_counter").innerText;
    contentDialog.appendChild(document.createTextNode("YOU WON. Your score: " + score));
    var cancelButtonElem = document.createElement("button");
    cancelButtonElem.className = "button_cancel";
    cancelButtonElem.onclick = dialogModule.removeDialog;
    cancelButtonElem.appendChild(document.createTextNode("Cancel"));
    dialogModule.enableDialog("Congrats", [contentDialog], [cancelButtonElem])
  }

  /**
   * Dispach event for when the game is won
   * @alias winGame
   */
  function winGame() {
    // Stop game
    game.isPlaying = false;
    clearIntervalTimeCounter();

    // Show win dialog
    createWinDialog()
  }

  /**
   * For mobile devices where is no right click, add a special button for next normal click to be registered as a marker
   * @alias handleClickAsMarker
   */
  function handleClickAsMarker() {
    game.isNormalClickMarker = true;
  }

  return {
    wipeCurrentGame: wipeCurrentGame,
    createNewGame: createNewGame,
    restartGame: restartGame,
    getAllNeighbours: getAllNeighbours,
    handleClickAsMarker: handleClickAsMarker
  }
})();

/**
 * Game content variable
 * MAIN OBJECT
 */
var game = gameCoreModule.wipeCurrentGame();
gameCoreModule.createNewGame();
