/**
 * General game settings variable
 * @alias gameSettings
 */
var gameSettings = {
  columns: 10,
  rows: 10,
  difficulty: 7
};

/**
 * Game Settings module containing all game settings methods
 * @alias gameSettingsModule
 * @returns {Object} - All functions available outside
 */
var gameSettingsModule = (function () {
  /**
   * Game Settings constants
   * @alias GAME_SETTINGS_DEFAULT
   */
  var GAME_SETTINGS_DEFAULT = {
    MIN_ROWS: 1,
    MAX_ROWS: 16,
    MIN_COLUMNS: 1,
    MAX_COLUMNS: 30,
    MIN_DIFFICULTY: 5,
    MAX_DIFFICULTY: 10
  }

  loadGameSettings();

  /**
   * Fill Dialog Content with specific infos
   * @alias fillDialogContentSettings
   * @param {Object} dialogContentInfo - Dialog content informations specifing boundries of game settings
   * @returns {Array} - A list of HTMLElements to be placed in dialog
   */
  function fillDialogContentSettings(dialogContentInfo) {
    var dialogElems = [];
    dialogContentInfo.forEach(function (info) {
      var formContentElem = document.createElement("div");
      formContentElem.appendChild(document.createTextNode(info.text));

      var spanContentElem = document.createElement("span");
      spanContentElem.id = "span_" + info.tag;
      formContentElem.appendChild(spanContentElem);

      var inputContentElem = document.createElement("input");
      inputContentElem.id = "input_" + info.tag;
      inputContentElem.setAttribute("type", "range");
      inputContentElem.setAttribute("min", info.rangeMin);
      inputContentElem.setAttribute("max", info.rangeMax);

      formContentElem.appendChild(inputContentElem);

      dialogElems.push(formContentElem);
    })
    return dialogElems;
  }

  /**
   * Fill dialog actions(buttons) for game settings
   * @alias fillDialogActionsSettings
   * @returns - List of HTMLElement to pe appended to dialog actions
   */
  function fillDialogActionsSettings() {
    var actionsElems = [];

    var cancelButtonElem = document.createElement("button");
    cancelButtonElem.className = "button_cancel";
    cancelButtonElem.onclick = dialogModule.removeDialog;
    cancelButtonElem.appendChild(document.createTextNode("Cancel"));
    actionsElems.push(cancelButtonElem);

    var saveButtonElem = document.createElement("button");
    saveButtonElem.onclick = saveGameSettings;
    saveButtonElem.appendChild(document.createTextNode("Save"));
    actionsElems.push(saveButtonElem);

    return actionsElems;
  }

  /**
   * Launch game settings
   * Open Game settings dialog
   * @alias openGameSettings
   */
  function openGameSettings() {
    // Create Dialog
    var dialogContentInfo = [{
        tag: "rows",
        text: "Rows: ",
        rangeMin: GAME_SETTINGS_DEFAULT.MIN_ROWS,
        rangeMax: GAME_SETTINGS_DEFAULT.MAX_ROWS
      },
      {
        tag: "columns",
        text: "Columns: ",
        rangeMin: GAME_SETTINGS_DEFAULT.MIN_COLUMNS,
        rangeMax: GAME_SETTINGS_DEFAULT.MAX_COLUMNS
      },
      {
        tag: "difficulty",
        text: "Difficulty: ",
        rangeMin: GAME_SETTINGS_DEFAULT.MIN_DIFFICULTY,
        rangeMax: GAME_SETTINGS_DEFAULT.MAX_DIFFICULTY
      }
    ];

    // Fill Content Dialog
    dialogModule.enableDialog("Game Settings", fillDialogContentSettings(dialogContentInfo), fillDialogActionsSettings());

    // Load saved game settings
    loadGameSettings();

    // Add auxiliary info/events for dialog
    dialogContentInfo.map(function (elem) {
      return elem.tag;
    }).forEach(function (tag) {
      var input = document.getElementById("input_" + tag);
      input.value = gameSettings[tag];
      var show = document.getElementById("span_" + tag);
      show.innerText = gameSettings[tag];

      input.addEventListener("input", function () {
        show.innerHTML = input.value;
      });

      input.addEventListener("change", function () {
        show.innerHTML = input.value;
      });
    })
  }

  /**
   * Load old gmae settings form local storage
   * @alias loadGameSettings
   */
  function loadGameSettings() {
    if (localStorage) {
      var loadTemp = localStorage.getItem("game_settings");
      gameSettings = loadTemp ? JSON.parse(loadTemp) : gameSettings;
    } else {
      console.log("No local storage found. Can't save game settings!")
    }
  }

  /**
   * Validate new game settings input
   * @alias validateGameSettings
   * @param {Object} gSettings - New game settings (same structure as global gameSettings) 
   */
  function validateGameSettings(gSettings) {
    if (gSettings.newRows < GAME_SETTINGS_DEFAULT.MIN_ROWS)
      gSettings.newRows = GAME_SETTINGS_DEFAULT.MIN_ROWS;

    if (gSettings.newRows > GAME_SETTINGS_DEFAULT.MAX_ROWS)
      gSettings.newRows = GAME_SETTINGS_DEFAULT.MAX_ROWS;

    if (gSettings.newColumns < GAME_SETTINGS_DEFAULT.MIN_COLUMNS)
      gSettings.newColumns = GAME_SETTINGS_DEFAULT.MIN_COLUMNS;

    if (gSettings.newColumns > GAME_SETTINGS_DEFAULT.MAX_COLUMNS)
      gSettings.newColumns = GAME_SETTINGS_DEFAULT.MAX_COLUMNS;

    if (gSettings.newDifficulty < GAME_SETTINGS_DEFAULT.MIN_DIFFICULTY)
      gSettings.newDifficulty = GAME_SETTINGS_DEFAULT.MIN_DIFFICULTY;

    if (gSettings.newDifficulty > GAME_SETTINGS_DEFAULT.MAX_DIFFICULTY)
      gSettings.newDifficulty = GAME_SETTINGS_DEFAULT.MAX_DIFFICULTY;

    return gSettings;
  }

  /**
   * Save game settings in game & local storage
   * @alias saveGameSettings
   */
  function saveGameSettings() {
    var newGameSettings = {
      rows: document.getElementById("span_rows").innerText,
      columns: document.getElementById("span_columns").innerText,
      difficulty: document.getElementById("span_difficulty").innerText,
    }

    gameSettings = validateGameSettings(newGameSettings);

    if (localStorage) {
      localStorage.setItem("game_settings", JSON.stringify(gameSettings))
    } else {
      console.log("No local storage found. Can't save game settings!")
    }

    // Remove Dialog
    dialogModule.removeDialog();
  }

  return {
    openGameSettings: openGameSettings,
    validateGameSettings: validateGameSettings,
    GAME_SETTINGS_DEFAULT: GAME_SETTINGS_DEFAULT
  }
})();
