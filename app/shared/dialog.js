/**
 * Dialog module containing all dialog functionality
 * @alias dialogModule
 * @returns {Object} - All functions available outside of dialog module
 */
var dialogModule = (function () {
  /**
   * Creates a new dialog template inside the body
   * Creates a new dialog template (Modal) to show information
   * If there is any other dialog opened, remove it.
   * @alias createDialogTemplate
   * @returns {Object} - dialog object with all it's features
  */
  function createDialogTemplate() {
    removeDialog();

    var dialogTemplate = document.createElement("div");
    dialogTemplate.id = "dialog";
    dialogTemplate.className = "dialog";

    var dialogCard = document.createElement("div");
    dialogCard.id = "dialog_card";
    dialogCard.className = "dialog_card";

    var dialogTitle = document.createElement("div");
    dialogTitle.id = "dialog_title";
    dialogTitle.className = "dialog_title";
    dialogCard.appendChild(dialogTitle);

    var dialogContent = document.createElement("div");
    dialogContent.id = "dialog_content";
    dialogContent.className = "dialog_content";
    dialogCard.appendChild(dialogContent);

    var dialogActions = document.createElement("div");
    dialogActions.id = "dialog_actions";
    dialogActions.className = "dialog_actions";
    dialogCard.appendChild(dialogActions);

    dialogTemplate.appendChild(dialogCard);
    document.body.appendChild(dialogTemplate);

    return {
      main: dialogTemplate,
      card: dialogCard,
      title: dialogTitle,
      content: dialogContent,
      actions: dialogActions
    }
  }

  /**
   * Render new dialog on demand
   * @alias enableDialog
   * @param {string} titleText - Dialog title
   * @param {Array} contentElems - Array of HTMLElement to pe appended to dialog content
   * @param {Array} actionsElems - Array of HTMLElement to pe appended to dialog actions
   */
  function enableDialog(titleText, contentElems, actionsElems) {
    var dialog = createDialogTemplate();

    // Create title
    var titleElem = document.createElement("p");
    var titleTextNode = document.createTextNode(titleText);
    titleElem.appendChild(titleTextNode);


    // Add elems to dialog
    dialog.title.appendChild(titleElem);
    if (contentElems) {
      contentElems.forEach(function (elem) {
        dialog.content.appendChild(elem);
      })
    }
    if (actionsElems) {
      actionsElems.forEach(function (elem) {
        dialog.actions.appendChild(elem);
      })
    }

    /**
     * Remove dialog if user clicks outside of dialog card
     */
    window.addEventListener("click", dialogEventClick);
  }

  function dialogEventClick(event) {
    if (event.target && event.target.id == "dialog")
      removeDialog();
  }

  /**
   * Remove current opened dialog
   * @alias Utils.removeDialog
   */
  function removeDialog() {
    var dialog = document.getElementById("dialog");
    if (dialog) {
      dialog.remove();
    }

    window.removeEventListener("click", dialogEventClick);
  }

  return {
    enableDialog: enableDialog,
    removeDialog: removeDialog
  }
})();
