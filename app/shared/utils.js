/**
 * Utils module containing all utils methods
 * @alias utilsModule
 * @returns {Object} - All functions available outside
 */
var utilsModule = (function () {
  /**
   * Polyfill: Add remove for IE
   */
  if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
      if (this.parentNode) {
        this.parentNode.removeChild(this);
      }
    };
  }

  /**
   * Remove all children of HTMLElement
   * @alias emptyElement
   * @param {HTMLElement} elem - Main element 
   */
  function emptyElement(elem) {
    if (elem)
      while (elem.firstChild) {
        elem.removeChild(elem.firstChild);
      }
  }

  /**
   * Shufle array elements
   * @alias shuffle
   * @param {Array} array - Array to be shuffled
   * @returns {Array} - The shuffled array
   */
  function shuffle(array) {
    var currentIndex = array.length;
    var temporaryValue;
    var randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  return {
    emptyElement: emptyElement,
    shuffle: shuffle
  }
})();
