# Minesweeper Clone

Repository containing my implementation of a well known game: Minesweeper.
This is a simple game created with vanila JS and no external modules. 
The UI theme is based on flat design UI.

## Tested on browsers (works)

* Chrome [Checked] (61.0.3163.100 (Official Build) (64-bit))
* Firefox [Checked] (56.0.1 (64-bit))
* Internet Explorer [Checked] (11.608.15063.0)
* MS Edge [Checked] (40.15.063)
* Chrome Mobile [Checked] (61.0.3163.73)

## Prerequisites

You need a web server to host the app. (Apache/NGINX/http-server (node.js module))


## Installing

Copy entire project into your `www` folder.


## Running the tests

Tests run automatically when app is initialized.


## Authors

* **Pultea Alexandru** - *Initial work* - [Nexflame](https://bitbucket.org/Nexflame)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by: Minesweeper Game
* Design: Flat UI
