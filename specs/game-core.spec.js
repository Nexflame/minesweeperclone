var specGameCore = (function () {
  var errors = [];

  function logError(msg) {
    errors.push(msg)
    errors.push("\n");
  }

  function testGetAllNeighbours() {
    defCol = game.playingBoard.columns;
    defRow = game.playingBoard.rows;
    game.playingBoard.columns = 30;
    game.playingBoard.rows = 30;


    var neighs = gameCoreModule.getAllNeighbours(0);
    if (neighs[0] != 1 ||
      neighs[1] != 30 ||
      neighs[2] != 31 ||
      neighs.length != 3) {
        logError("[TEST FAILED] Error in getting square neighbours for 0")
    }

    neighs = gameCoreModule.getAllNeighbours(14);
    if (neighs[0] != 13 ||
      neighs[1] != 15 ||
      neighs[2] != 43 ||
      neighs[3] != 44 ||
      neighs[4] != 45 ||
      neighs.length != 5) {
        logError("[TEST FAILED] Error in getting square neighbours for 14")
    }

    neighs = gameCoreModule.getAllNeighbours(899);
    if (neighs[0] != 868 ||
      neighs[1] != 869 ||
      neighs[2] != 898 ||
      neighs.length != 3) {
        logError("[TEST FAILED] Error in getting square neighbours for 14")
    }

    game.playingBoard.columns = defCol;
    game.playingBoard.rows = defRow;
  }

  return {
    runAll: function () {
      testGetAllNeighbours();
    },
    getErrors: function () {
      return errors;
    }
  }
})();

