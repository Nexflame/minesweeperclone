var specGameSettings = (function () {
  var errors = [];

  function logError(msg) {
    errors.push(msg)
    errors.push("\n");
  }

  function testValidateGameConfigLow() {
    var gSettings = {
      newRows: -1,
      newColumns: 2,
      newDifficulty: 3,
    }

    gameSettingsModule.validateGameSettings(gSettings);

    if (gSettings.newRows != 1 ||
      gSettings.newColumns != 2 ||
      gSettings.newDifficulty != 5) {
      logError("[TEST FAILED] Game settings validation failed! settings: [" + JSON.stringify(gSettings) + "] expected: 1, 2, 3");
    }
  }

  function testValidateGameConfigHigh() {
    var gSettings = {
      newRows: 100,
      newColumns: 100,
      newDifficulty: 100,
    }

    gameSettingsModule.validateGameSettings(gSettings);

    if (gSettings.newRows != 16 ||
      gSettings.newColumns != 30 ||
      gSettings.newDifficulty != 10) {
      logError("[TEST FAILED] Game settings validation failed! settings: [" + JSON.stringify(gSettings) + "] expected: 16, 30, 10");
    }
  }

  function testDefaultGameConfigVal() {
    if (!gameSettingsModule.GAME_SETTINGS_DEFAULT) {
      logError("[TEST FAILED] No default game config");
    }

    if (gameSettingsModule.GAME_SETTINGS_DEFAULT.MIN_ROWS != 1 ||
      gameSettingsModule.GAME_SETTINGS_DEFAULT.MAX_ROWS != 16 ||
      gameSettingsModule.GAME_SETTINGS_DEFAULT.MIN_COLUMNS != 1 ||
      gameSettingsModule.GAME_SETTINGS_DEFAULT.MAX_COLUMNS != 30 ||
      gameSettingsModule.GAME_SETTINGS_DEFAULT.MIN_DIFFICULTY != 5 ||
      gameSettingsModule.GAME_SETTINGS_DEFAULT.MAX_DIFFICULTY != 10) {
      logError("[TEST FAILED] Default game config is not fitting the requirements" + JSON.stringify(gameSettingsModule.GAME_SETTINGS_DEFAULT));
    }
  }

  return {
    runAll: function () {
      testValidateGameConfigLow();
      testValidateGameConfigHigh();
      testDefaultGameConfigVal();
    },
    getErrors: function () {
      return errors;
    }
  }
})();

