var specs = (function () {
  var errors = [];
  
  function showErrorDialog() {
    var contentDialog = document.createElement("div");
    contentDialog.style.whiteSpace = "pre-wrap";
    contentDialog.appendChild(document.createTextNode(errors.toString()));
    var cancelButtonElem = document.createElement("button");
    cancelButtonElem.className = "button_cancel";
    cancelButtonElem.onclick = dialogModule.removeDialog;
    cancelButtonElem.appendChild(document.createTextNode("Cancel"));
    dialogModule.enableDialog("Tests error", [contentDialog], [cancelButtonElem]);
  }

  return {
    runAll: function () {
      specUtils.runAll();
      specGameSettings.runAll();
      specGameCore.runAll();
      
      errors = errors.concat(specUtils.getErrors()).concat(specGameSettings.getErrors()).concat(specGameCore.getErrors());

      if (errors.length)
        showErrorDialog()
    },
    getErrors: function() {
      return errors;
    }
  }
})();

specs.runAll();

