var specUtils = (function () {
  var errors = [];
  
  function logError(msg) {
    errors.push(msg)
    errors.push("\n");
  }

  function testShuffle() {
    var initArr = [1, 2, 3, 4, 5, 6, 7, 8];
    var shufArr = [1, 2, 3, 4, 5, 6, 7, 8];

    var isOk = false;
    // Run test 10 times cause it can return same array after shuffling
    for (var i = 0; i < 10; ++i) {
      var tempOk = false;
      shufArr = utilsModule.shuffle(shufArr);
      for (var j = 0; j < initArr.length; ++j) {
        if (initArr[j] != shufArr[j]) {
          tempOk = true;
        }
      }

      if (tempOk) {
        isOk = true;
      }
    }

    if (!isOk) {
      logError("[TEST FAILED] Shuffle mechanism is not working! Arrays 1: [" + initArr.toString() +  "] 2: [" + shufArr.toString() + "]")
    }
  }

  return {
    runAll: function () {
      testShuffle();
    },
    getErrors: function () {
      return errors;
    }
  }
})();

